FROM archlinux:base-devel
LABEL org.opencontainers.image.authors="rene.pasold@jena.de"

ENV LC_ALL "de_DE.UTF-8"
ENV container docker
ENV TZ UTC

ARG user=r

RUN cat /proc/meminfo \
 && sed -i 's/NoExtract/#NoExtract/g' /etc/pacman.conf \
 && echo -e "\n[archpkgs]\nSigLevel = Optional TrustedOnly\nServer = https://garbuszus.de/archpkgs/\$arch\n" >> /etc/pacman.conf \
 && sed -i 's/#de_DE.UTF-8/de_DE.UTF-8/g' /etc/locale.gen \
 && echo "LANG=de_DE.UTF-8" > /etc/locale.conf \
 && echo -e "TZ=UTC\ncontainer=docker\n" >> /etc/environment \
 && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone \
 && pacman -Syy \
 && pacman -S --noconfirm glibc \
 && locale-gen \
 && pacman -S --needed --noconfirm git \
 && pacman -Scc --noconfirm

# Installation user
RUN useradd --system --create-home install \
 && echo "install ALL=(ALL:ALL) NOPASSWD:ALL" > /etc/sudoers.d/install
USER install
WORKDIR /home/install

# Install yay
RUN git clone https://aur.archlinux.org/yay.git \
  && cd yay \
  && makepkg -sri --needed --noconfirm --nocheck \
  && yay -Scc --noconfirm

RUN yay -S --needed --noconfirm --mflags "--nocheck" r msodbcsql17 \
 && yay -Scc --noconfirm

# libraries & tools
RUN yay -S --needed --noconfirm --repo --mflags "--nocheck" \
    librsvg libsodium libpqxx poppler \
    arrow cfitsio libheif podofo cmake \
    gcc-fortran gdal netcdf libjxl libwebp \
    hdf5 mariadb-libs openexr v8-r imagemagick \
    pipewire-jack gifski ffmpeg \
    tesseract tesseract-data-deu tesseract-data-eng \
    extra/rust downgrade \
 && yay -Scc --noconfirm

COPY resources/packagelist-arch /home/install

# AUR
RUN yay -S --needed --noconfirm --mflags "--nocheck" $(cat /home/install/packagelist-arch) \
 && yay -Scc --noconfirm

USER root
WORKDIR /root/

RUN sed -i 's/ODBC Driver 17 for SQL Server/SQL Server/g' /etc/odbcinst.ini

# R-User
RUN useradd --system --create-home $user
USER $user
WORKDIR /home/$user

ARG CRAN_MIRROR="https://ftp.gwdg.de/pub/misc/cran/"

COPY --chown=$user:$user resources/.Rprofile /home/$user/
COPY --chown=$user:$user resources/.Renviron /home/$user/
COPY --chown=$user:$user resources/packagelist-cran /home/$user/packagelist
COPY --chown=$user:$user resources/install-packages.R /home/$user/
RUN echo "CRAN_MIRROR=${CRAN_MIRROR}" >> .Renviron \
 && mkdir -p ~/.Rlib \
 && Rscript /home/$user/install-packages.R \
    | grep --line-buffered -e "* DONE" -e "begin installing" -e "# " -e "fatal error" -e "terminated" -e "Error 1" \
    | awk '{ print strftime("%Y-%m-%d %H:%M:%S"), $0; fflush() }'

COPY --chown=$user:$user resources/installed-packages.R /home/$user/
RUN Rscript /home/$user/installed-packages.R

USER $user
