# r_docker

## Description
This project provides a dockerfile which in turn can be used to build a base image for R-based projects, e.g. a Shiny Server (https://gitlab.opencode.de/r-statistik/shiny_docker). The image can of course also be used for anything else R-related.

## Usage
As per usual basic usage is `docker build -t <repo>` and `docker run <repo>`, usually the project will be used in as a base image though.

    FROM r-statistik/r_docker:<version>

When building the image locally, a CRAN mirror can be set using `--build-arg CRAN_MIRROR=<url>`.

## License
Eclipse Public License - v 2.0
